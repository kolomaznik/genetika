# Online verze

Online verze programu je dostupná na adrese: http://bioinformatics.ibp.cz:8080/

# Installation

config.properties

## Local

## Server

----

# Verze: 2.5
1. Rozšíření výpočtu energie genomu [#42]

# Verze: 2.4
1. Přidána možnost analýzy kruhového genomu [#25]
2. Zavedeni jasDB databáze pro ukládání genomu, dat uživatelů a výsledků.
3. Doplnění možnosti pouštění aplikace ve **standalone** a a **server** variantě. Liší se přístupem k uživateli
4. Zavedení uživatelských účtů.

# Verze: 2.3
1. Přidání p53 binding predictor
2. Možnost zadat genom jako text
3. Odstranění přebytečných závislostí
4. Omezení výstupu + nastavení v konfiguraci

# Verze: 2.2
1. Implementace importu ve FASTA formátu
    - Znaky mimo ACTG při porovnávání palindromu vždy generují mismatches
2. Opravy chyb
    - [#14]: implementace pro Plain a Fasta
    - [#15]: odstranění mezer se provádí při importech
    - [#17]: odstraněno genumu z paměti, načítá se při každém dotazu.
    - [#18]: náš program odstraňuje palindromy, které jsou součástí jiných palindromů.

# Verze: 2.1
1. Vylepšeno užiatelské rozhraní
    - Filtrování 
    - Oprava chyb při změnách filtrů
    - Opraveno odesilání genomu pod chome
2. Implementece odesílá plain DNA s automatickýcm odstraňováním prázných znaků.
3. První verze implamnece importu FASTA podle kodovaní: Amino Acid Code

# Verze 2.0
* Totální refaktoring jak rozgraní tak samotné logiky aplikace.