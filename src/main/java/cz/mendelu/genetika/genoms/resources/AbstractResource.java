package cz.mendelu.genetika.genoms.resources;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Honza on 23.01.2016.
 */
public abstract class AbstractResource {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractResource.class);

    private static final String METHOD = "GET";

    protected InputStream getResource(String urlAddress) {
        try {
            LOG.info("Start get resource from {}.", urlAddress);
            URL url = new URL(urlAddress);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(METHOD);
            urlConnection.connect();

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                //redirekt
                if(urlConnection.getResponseCode() != HttpURLConnection.HTTP_MOVED_PERM || urlConnection.getResponseCode() != HttpURLConnection.HTTP_MOVED_TEMP) {
                    String newUrl = urlConnection.getHeaderField("Location");
                    return this.getResource(newUrl);
                } else {
                    throw new ResourceException(String.format("Get resource failed. Server response code %d, content:\n%s", IOUtils.toString(urlConnection.getInputStream())));
                }
            }

            return urlConnection.getInputStream();
        } catch (Exception e) {
            throw new ResourceException(String.format("Get resource from url %s failed.", urlAddress), e);
        }
    }
}
