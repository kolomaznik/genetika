package cz.mendelu.genetika;

import cz.mendelu.genetika.dao.jasdb.EmbeddedJasDB;
import cz.mendelu.genetika.rest.jetty.EmbeddedJetty;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.net.URL;

/**
 * Created by xkoloma1 on 04.01.2016.
 */
public class Start {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_GREEN = "\u001B[32m";
    private static final Logger LOG = LoggerFactory.getLogger("Genetika");
    private static Service jasDBService;
    private static Service jettyService;

    public static void main(String ... args) throws Exception {

        System.out.println(ANSI_GREEN + "   ____                 _   _ _         \n" +
                "  / ___| ___ _ __   ___| |_(_) | ____ _ \n" +
                " | |  _ / _ \\ '_ \\ / _ \\ __| | |/ / _` |\n" +
                " | |_| |  __/ | | |  __/ |_| |   < (_| |\n" +
                "  \\____|\\___|_| |_|\\___|\\__|_|_|\\_\\__,_|\n" +
                ANSI_YELLOW + "=========================================\n" + ANSI_RESET);

        // Create jasDB threads;
        if (Config.db.Type.JASDB_EMBEDDED == Config.db.type()) {
            jasDBService = EmbeddedJasDB.getEmbeddedJasDB();
            synchronized (jasDBService.getStartUpLock()) {
                jasDBService.start();
                jasDBService.getStartUpLock().wait();
            }
        } else {
            throw ConfigException.unsupportedConfigration("db.type", Config.db.type());
        }

        // Create jetty threads;
        jettyService = EmbeddedJetty.getEmbeddedJetty();
        synchronized (jettyService.getStartUpLock()) {
            jettyService.start();
            jettyService.getStartUpLock().wait();
        }

        // Join main thread to jetty
        LOG.info("ready for usage...\n{}", StringUtils.repeat("=", 40));

        if (Config.app.Mode.DESKTOP == Config.app.mode()) {
            showInWebBrowse();
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            jasDBService.terminate();
            jettyService.terminate();
        }));

        System.in.read();
        System.exit(0);

    }

    private static void showInWebBrowse() {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(new URL("http://localhost:8080").toURI());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static abstract class Service extends Thread {

        protected final Object startUpMonitor = new Object();

        protected boolean ready = false;

        protected Service(String name) {
            super(name);
        }

        public boolean isReady() {
            return ready;
        }

        public Object getStartUpLock() {
            return startUpMonitor;
        }

        public abstract void terminate();
    }


}
