package cz.mendelu.genetika.bulk.analysis;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import cz.mendelu.genetika.Config;
import cz.mendelu.genetika.genoms.Feature;
import cz.mendelu.genetika.genoms.FeatureTable;
import cz.mendelu.genetika.genoms.Sequence;
import cz.mendelu.genetika.genoms.helpers.FeatureSieve;
import cz.mendelu.genetika.genoms.helpers.NCBIMultiDownloader;
import cz.mendelu.genetika.genoms.helpers.PalindromeMatcherHelper;
import cz.mendelu.genetika.genoms.helpers.SequenceHelper;
import cz.mendelu.genetika.genoms.resources.NCBI;
import cz.mendelu.genetika.palindrome.*;
import cz.mendelu.genetika.rest.jetty.JettyContext;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Jiří Lýsek on 16.11.2016.
 */
public class BulkAnalysis {

    private PalindromeDetectorBuilder palindromeDetectorBuilder = JettyContext.getPalindromeDetectorBuilder();

    private static final Logger LOG = LoggerFactory.getLogger(BulkAnalysis.class);

    private String pathToCsv;
    private String storagePath;

    private int minLength = 6;
    private int maxLength = 30;

    private BulkCSVWriter overallReport;
    private FeatureCSVWriter overallFeatureReport;

    private DecimalFormat df;

    public BulkAnalysis(String pathToCsv, String storagePath) {
        this.pathToCsv = pathToCsv;
        this.storagePath = storagePath;

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
        otherSymbols.setDecimalSeparator('.');
        df = new DecimalFormat("#.########", otherSymbols);
    }

    public void run() throws IOException {
        Stream<Pair<String, String>> idStream = download();
        overallReport = new BulkCSVWriter(df, storagePath, minLength, maxLength);
        overallReport.prepareOverallReport();
        overallFeatureReport = new FeatureCSVWriter(df, storagePath);
        idStream.forEach(info -> {
            try {
                Sequence s = new Sequence(new File(this.storagePath + "/" + info.getKey() + NCBIMultiDownloader.DOWNLOADED_FILE_EXT));
                PalindromeMatcher pm = runPalindromeAnalysis(s);
                LOG.info("Found {} palindromes for {}", pm.getCount(), info.getKey());

                //pro kazdou feature v genu mam roztridene palindromy
                Stream<Pair<Feature, FeatureSieve<Palindrome>>> sievedPalindromes = findPalindromesAroundFeatures(info.getKey(), pm);
                storeFeatureStatistics(info.getKey(), sievedPalindromes);

                Map<Integer, Integer> hist = PalindromeMatcherHelper.calcHistogramLength(pm);
                storeHistogramLength(info.getKey(), hist);

                storePalindromes(info.getKey(), pm);

                overallReport.storeOverallInfo(
                        info.getValue(),
                        info.getKey(),
                        s.getLength(),
                        pm.getCount(),
                        PalindromeMatcherHelper.getCount(pm, 8).intValue(),
                        PalindromeMatcherHelper.getCount(pm, 10).intValue(),
                        PalindromeMatcherHelper.getCount(pm, 12).intValue(),
                        SequenceHelper.count(s, Sequence.G) + SequenceHelper.count(s, Sequence.C),
                        hist);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        overallReport.finish();
        overallFeatureReport.store();
    }

    private void storeFeatureStatistics(String id, Stream<Pair<Feature, FeatureSieve<Palindrome>>> sievedPalindromes) throws IOException {
        FileWriter fw = new FileWriter(this.storagePath + "/" + id + "_feature_palindromes.csv");
        CSVWriter writer = new CSVWriter(new BufferedWriter(fw));

        String[] header = new String[] {
                "Feature",
                "Info",
                "Feature start",
                "Feature end",
                "Feature size",
                "fr. all inside",
                "fr. 8+ inside",
                "fr. 10+ inside",
                "fr. 12+ inside",
                "fr. all around",
                "fr. 8+ around",
                "fr. 10+ around",
                "fr. 12+ around",
                "fr. all before",
                "fr. 8+ before",
                "fr. 10+ before",
                "fr. 12+ before",
                "fr. all after",
                "fr. 8+ after",
                "fr. 10+ after",
                "fr. 12+ after",
                "all inside",
                "8+ inside",
                "10+ inside",
                "12+ inside",
                "all around",
                "8+ around",
                "10+ around",
                "12+ around",
                "all before",
                "8+ before",
                "10+ before",
                "12+ before",
                "all after",
                "8+ after",
                "10+ after",
                "12+ after",
        };
        writer.writeNext(header);

        sievedPalindromes.forEach(pair -> {
            Feature f = pair.getKey();
            FeatureSieve<Palindrome> sieve = pair.getValue();

            overallFeatureReport.addFeature(f, sieve);

            try {
                writer.writeNext(new String[] {
                        f.getName(),
                        f.concatQualifiers(),
                        String.valueOf(f.getPosition() + 1),
                        String.valueOf(f.getPosition() + f.getLength() + 1),
                        String.valueOf(f.getLength()),
                        //frekvence v miste
                        df.format(sieve.getCount(0,0) / (double)f.getLength()),
                        df.format(sieve.getCount(0,8) / (double)f.getLength()),
                        df.format(sieve.getCount(0,10) / (double)f.getLength()),
                        df.format(sieve.getCount(0,12) / (double)f.getLength()),
                        //frekvence v okoli 100
                        df.format(sieve.getCount(100) / (2 * (double)Feature.COUNT_DISTANCE)),
                        df.format(sieve.getCount(100,8) / (2 * (double)Feature.COUNT_DISTANCE)),
                        df.format(sieve.getCount(100,10) / (2 * (double)Feature.COUNT_DISTANCE)),
                        df.format(sieve.getCount(100,12) / (2 * (double)Feature.COUNT_DISTANCE)),
                        //frekvence v okoli 100 pred
                        df.format(sieve.getCountBefore(100) / (double)Feature.COUNT_DISTANCE),
                        df.format(sieve.getCountBefore(100,8) / (double)Feature.COUNT_DISTANCE),
                        df.format(sieve.getCountBefore(100,10) / (double)Feature.COUNT_DISTANCE),
                        df.format(sieve.getCountBefore(100,12) / (double)Feature.COUNT_DISTANCE),
                        //frekvence v okoli 100 za
                        df.format(sieve.getCountAfter(100) / (double)Feature.COUNT_DISTANCE),
                        df.format(sieve.getCountAfter(100,8) / (double)Feature.COUNT_DISTANCE),
                        df.format(sieve.getCountAfter(100,10) / (double)Feature.COUNT_DISTANCE),
                        df.format(sieve.getCountAfter(100,12) / (double)Feature.COUNT_DISTANCE),
                        //pocty v miste
                        String.valueOf(sieve.getCount(0)),
                        String.valueOf(sieve.getCount(0,8)),
                        String.valueOf(sieve.getCount(0,10)),
                        String.valueOf(sieve.getCount(0,12)),
                        //pocty v okoli 100
                        String.valueOf(sieve.getCount(100)),
                        String.valueOf(sieve.getCount(100,8)),
                        String.valueOf(sieve.getCount(100,10)),
                        String.valueOf(sieve.getCount(100,12)),
                        //pocty v okoli 100 pred
                        String.valueOf(sieve.getCountBefore(100)),
                        String.valueOf(sieve.getCountBefore(100,8)),
                        String.valueOf(sieve.getCountBefore(100,10)),
                        String.valueOf(sieve.getCountBefore(100,12)),
                        //pocty v okoli 100 za
                        String.valueOf(sieve.getCountAfter(100)),
                        String.valueOf(sieve.getCountAfter(100,8)),
                        String.valueOf(sieve.getCountAfter(100,10)),
                        String.valueOf(sieve.getCountAfter(100,12)),
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        writer.close();
    }

    private Stream<Pair<Feature, FeatureSieve<Palindrome>>> findPalindromesAroundFeatures(String id, PalindromeMatcher pm) throws IOException {
        FeatureTable ft = new FeatureTable(new File(this.storagePath + "/" + id + "_ft" + NCBIMultiDownloader.DOWNLOADED_FILE_EXT));
        Stream<Pair<Feature, FeatureSieve<Palindrome>>> pairs = ft.loadFeatureDescriptions().map(f -> {
            FeatureSieve<Palindrome> sieve = new FeatureSieve<Palindrome>(f);
            sieve.addRange(0);
            sieve.addRange(f.COUNT_DISTANCE);
            //najit palindromy v okoli teto feature
            pm.forEach(p -> {
                sieve.decideMembership(p);
            });
            return new Pair<Feature, FeatureSieve<Palindrome>>(f, sieve);
        });
        return pairs;
    }

    private Map<String, String> getIDsFromCSV() throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(this.pathToCsv));
        CSVReader reader = new CSVReader(fileReader, '\t');
        List<String[]> rows = reader.readAll();
        reader.close();

        Map<String, String> ret = new HashMap<String, String>();
        for (String[] row : rows) {
            Pattern p = Pattern.compile("NC_[0-9]+(\\.[0-9]+){0,1}");
            if (p.matcher(row[1]).matches()) {
                ret.put(row[1], row[0]);
            } else {
                LOG.info("Not matched ID '{}' for {}", row[1], row[0]);
            }
        }
        return ret;
    }

    private Stream<Pair<String, String>> download() throws IOException {
        NCBI ncbiGenomes = new NCBI(
                Config.ncbi.restriction_timing(),
                Config.ncbi.url(),
                Config.ncbi.db(),
                Config.ncbi.retmode(),
                Config.ncbi.rettype()
        );

        NCBI ncbiFeatureTable = new NCBI(
                Config.ncbi.restriction_timing(),
                Config.ncbiFeatureTable.url(),
                Config.ncbiFeatureTable.db(),
                Config.ncbiFeatureTable.retmode(),
                Config.ncbiFeatureTable.rettype()
        );

        Map<String, String> genomeIDs = this.getIDsFromCSV();
        NCBIMultiDownloader downloader = new NCBIMultiDownloader(this.storagePath, ncbiGenomes, ncbiFeatureTable);
        downloader.init();
        return downloader.downloadGenomes(genomeIDs.keySet()).map(id -> new Pair(id, genomeIDs.get(id)));
    }

    private PalindromeMatcher runPalindromeAnalysis(Sequence s) throws IOException {
        PalindromeDetector detector = makePalindromeDetector();
        PalindromeMatcher pm = detector.findPalindrome(s);
        return pm;
    }

    /**
     * ulozit pocty a delky inverznich repetic
     */
    private void storeHistogramLength(String id, Map<Integer, Integer> hist) throws IOException {
        FileWriter f = new FileWriter(this.storagePath + "/" + id + "_hist_length.csv");
        BufferedWriter fileWriter = new BufferedWriter(f);
        CSVWriter writer = new CSVWriter(fileWriter);
        writer.writeNext(new String[]{"Size of palindrome", "Amount"});
        for (Integer size : hist.keySet()) {
            writer.writeNext(new String[]{String.valueOf(size), String.valueOf(hist.get(size))});
        }
        writer.close();
    }

    private int calcGC(Sequence s) {
        int ret = 0;
        for (int i = 0; i < s.getLength(); i++) {
            String gene = s.getSequence(i, 1).toString();
            if (gene.equals(Sequence.G) || gene.equals(Sequence.C)) {
                ret++;
            }
        }
        return ret;
    }

    private PalindromeDetector makePalindromeDetector() {
        NumberRange size = new NumberRange(minLength, maxLength);
        NumberRange spacer = new NumberRange("0-10");
        NumberRange mismatches = new NumberRange("0,1");
        return palindromeDetectorBuilder.getPalindromeDetector(size.getValues(), spacer.getValues(), mismatches.getValues());
    }

    /**
     * ulozit nalezene palindromy
     *
     * @param pm
     */
    private void storePalindromes(String id, PalindromeMatcher pm) throws IOException {
        List<Palindrome> palindromes = pm.stream().sorted((p1, p2) -> {
            return Long.compare(p1.getPosition(), p2.getPosition());
        }).collect(Collectors.toList());

        FileWriter f = new FileWriter(this.storagePath + "/" + id + "_palindromes.csv");
        BufferedWriter fileWriter = new BufferedWriter(f);
        CSVWriter palindromeCSV = new CSVWriter(fileWriter);
        palindromeCSV.writeNext(new String[]{
                "Signature",
                "Position",
                "Length",
                "Spacer length",
                "Mismatch count",
                "Sequence",
                "Spacer",
                "Opposite"
        });
        palindromes.stream().forEach(palindrome -> {
            palindromeCSV.writeNext(new String[]{
                    palindrome.getSequence().getLength() + "-" + palindrome.getSpacer().getLength() + "-" + palindrome.getMismatches(),
                    (palindrome.getPosition() + 1) + "",
                    palindrome.getSequence().getLength() + "",
                    palindrome.getSpacer().getLength() + "",
                    palindrome.getMismatches() + "",
                    palindrome.getSequence().toString(),
                    palindrome.getSpacer().toString(),
                    palindrome.getOpposite().toString()
            });
        });
        fileWriter.close();
    }

    public static void main(String[] args) {
        String[] files = new String[]{
                "animals_amphibians",
                "animals_birds",
                //"animals_fishes",
                "animals_flatworms",
                //"animals_insects",
                "animals_mammals",
                "animals_reptiles",
                "animals_roundworms",
                "fungi_ascomycetes",
                "fungi_basidiomycetes",
                "other",
                //"other_animals",
                "other_fungi",
                "other_protists",
                "plants_green_algae",
                //"plants_land_plants",
                "plants_other",
                "protists_apicomplexans",
                //"all"
                //"test"*/
        };
        for (String batchFile : files) {
            BulkAnalysis ba = new BulkAnalysis("data/hromadna_analyza_1/" + batchFile + ".csv", "data/hromadna_analyza_1/results/" + batchFile);
            try {
                ba.run();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
